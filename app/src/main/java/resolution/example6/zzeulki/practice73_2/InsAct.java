package resolution.example6.zzeulki.practice73_2;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class InsAct extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ins);
    }

    public void onClick(View v) {
        MyContentProvider c = new MyContentProvider();

        EditText t = null;
        t = (EditText) findViewById(R.id.editText0);
        while(t.getText().length()>10)
            t.setText("글자가 너무 많습니다");
        String student_no = t.getText().toString();
        t = (EditText) findViewById(R.id.editText1);
        while(t.getText().length()>20)
            t.setText("글자가 너무 많습니다");
        String name = t.getText().toString();
        t = (EditText) findViewById(R.id.editText2);
        while(t.getText().length()>200)
            t.setText("글자가 너무 많습니다");
        String age = t.getText().toString();



        //String sql = "INSERT INTO people (id2,name,age) values (" + student_no + ",'" + name + "'," + age + ");";
        ContentValues DD = null;

        DD.put("id2",student_no);
        DD.put("name",name);
        DD.put("age",age);
        c.insert(null,DD);
//        SQLiteDatabase db = openOrCreateDatabase(
//                "test.db",
//                SQLiteDatabase.CREATE_IF_NECESSARY,
//                null
//        );
//
//        db.execSQL(sql);

        finish();
    }


}

class PeopleDBHelper extends SQLiteOpenHelper {
    public PeopleDBHelper(Context context){
        super(context,"test.db",null,1);
    }

    public void onCreate(SQLiteDatabase db){
        db.execSQL("create table if not exists people" + " (_id integer primary key autoincrement,id2 integer, name text, age integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table if exists people;");
        onCreate(db);

    }
}
