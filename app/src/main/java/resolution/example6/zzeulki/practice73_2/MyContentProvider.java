package resolution.example6.zzeulki.practice73_2;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;

public class MyContentProvider extends ContentProvider {
    static final Uri CONTENT_URI = Uri.parse("content://practice8_1.test/people");
    static final int GETALL = 1;
    static final int GETONE = 2;

    static final UriMatcher matcher;

    static{
        matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI("practice8_1.test","people",GETALL);
        matcher.addURI("practice8_1.test","people/*",GETONE);
    }

    SQLiteDatabase mDB;
    class TestDBHelpler extends SQLiteOpenHelper{
        public TestDBHelpler(Context c){
            super(c,"test.db",null,1);
        }

        public void onCreate(SQLiteDatabase db){
            db.execSQL("create table if not exists people" + "(_id integer primary key autoincrement, id2 integer, name text, age integer);");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer){
            db.execSQL("drop table if exists people");
            onCreate(db);
        }
    }

    public MyContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int cnt = 0;

        switch(matcher.match(uri)){
            case GETALL :
                cnt = mDB.delete("people",selection,selectionArgs);
                break;
            case GETONE :
                String where = "id2 = '" + uri.getPathSegments().get(1) + "'";
                if(TextUtils.isEmpty(selection) == false){
                    where += "AND" + selection;
                }
                cnt = mDB.delete("people",where,selectionArgs);
                break;
        }

        getContext().getContentResolver().notifyChange(uri,null);

        return cnt;
    }

    @Override
    public String getType(Uri arg0) {
       switch(matcher.match(arg0)){
           case GETALL :
               return "vnd.android.cursor.dir/vnd.vr.people";
           case GETONE :
               return "vnd.android.cursor.item/vnd.vr.people";
       }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long row = mDB.insert("people",null,values);

        if(row>0){
            Uri notiuri = ContentUris.withAppendedId(CONTENT_URI,row);
            getContext().getContentResolver().notifyChange(notiuri,null);

            return notiuri;
        }

        return null;
    }

    @Override
    public boolean onCreate() {
        TestDBHelpler helper = new TestDBHelpler(getContext());
        mDB = helper.getWritableDatabase();

        return (mDB != null);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
       String sql;
        sql = "select * from people";

        if(matcher.match(uri) == GETONE){
            sql += " where name =' ";
            sql +=  uri.getPathSegments().get(1);
            sql += "'";
        }

        sql += ";";

        Cursor cur = mDB.rawQuery(sql,null);
        return cur;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
       int cnt = 0;

        switch(matcher.match(uri)) {
            case GETALL:
                cnt = mDB.update("people", values, selection, selectionArgs);
                break;
            case GETONE:
                String where = "id2 = '" + uri.getPathSegments().get(1) + "'";
                if (TextUtils.isEmpty(selection) == false) {
                    where += " and " + selection;
                }
                cnt = mDB.update("people", values, where, selectionArgs);
                break;
        }

        getContext().getContentResolver().notifyChange(uri,null);
        return cnt;
    }
}
