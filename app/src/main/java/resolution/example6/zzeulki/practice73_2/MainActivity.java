package resolution.example6.zzeulki.practice73_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showListAct(View v)
    {
        Intent i = new Intent(this, ListAct.class);
        startActivity(i);
    }

    public void showInsAct(View v)
    {
        Intent i = new Intent(this, InsAct.class);
        startActivity(i);
    }

    public void showDelete(View v)
    {
        Intent i = new Intent(this, delete.class);
        startActivity(i);
    }

    public void showUpdate(View v)
    {
        Intent i = new Intent(this, Edit.class);
        startActivity(i);
    }

}
