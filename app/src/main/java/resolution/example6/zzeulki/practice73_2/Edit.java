package resolution.example6.zzeulki.practice73_2;

import android.app.ListActivity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;

public class Edit extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        loadDB();
    }
    @Override
    public void onResume(){
        super.onResume();
        loadDB();
    }

    public void loadDB()
    {
        SQLiteDatabase db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("create table if not exists people " + "(_id integer primary key autoincrement, id2 integer, name text, age integer);");

        Cursor c = db.rawQuery("select * from people;",null);
        startManagingCursor(c);

        //Log.i("sk", c.getString(0));

        ListAdapter adapt = new SimpleCursorAdapter(
                this,
                R.layout.item,
                c,
                new String[]{"id2","name","age"},
                new int[] {R.id.t1,R.id.t2,R.id.t3},0
        );

        setListAdapter(adapt);

        if(db!=null){
            db.close();
        }


    }

    public void onClick(View v) {

        MyContentProvider c = new MyContentProvider();

        EditText t = null;
        t = (EditText) findViewById(R.id.editText0);
        String student_no = t.getText().toString();
        t = (EditText) findViewById(R.id.editText1);
        String name = t.getText().toString();
        t = (EditText) findViewById(R.id.editText2);
        String age = t.getText().toString();

        c.CONTENT_URI.getPathSegments().add(1,student_no);

        ContentValues DD = null;

        DD.put("id2",student_no);
        DD.put("name",name);
        DD.put("age",age);

        c.update(c.CONTENT_URI,DD,null,null);
//        String sql = "UPDATE people SET name = '" +name+ "', age = "+age+" where id2 = "+student_no+";";
//
//        //String sql = "delete from people where name == '"+name+"';";
//        SQLiteDatabase db = openOrCreateDatabase(
//                "test.db",
//                SQLiteDatabase.CREATE_IF_NECESSARY,
//                null
//        );
//
//        db.execSQL(sql);

        finish();
    }
}
